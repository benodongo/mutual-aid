﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MutualAid.Models.Admin
{
    public class SetUpModels
    {
    }
    public class RateViewModel
    {
        public Guid id { get; set; }
        public decimal BonusRate { get; set; }

    }
    public class SetUpViewModel
    {
        public System.Guid Id { get; set; }
        public decimal DepositMin { get; set; }
        public decimal DepositMax { get; set; }
        public int MaturityDuration { get; set; }
        public int WithrawalConfirmDuration { get; set; }
        public int PayDuration { get; set; }
        public string AddedBy { get; set; }
        public System.DateTime DateAdded { get; set; }
    }
}