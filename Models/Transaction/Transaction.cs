﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MutualAid.Models.Transaction
{
    public class Transaction
    {
    }
    public class DepositViewModel
    {
        public System.Guid Id { get; set; }
        public decimal Amount { get; set; }
        public bool IsPaid { get; set; }
        public bool IsMature { get; set; }
        public DepositType DepositType { get; set; }
        public System.DateTime DateDeposited { get; set; }
    }
    public enum DepositType
    {
        InitialDeposit,
        ReDeposit
    }
    public  class WithdrawViewModel
    {
        public System.Guid Id { get; set; }
        public System.Guid DepositId { get; set; }
        public decimal Amount { get; set; }
        public decimal Redeposit { get; set; }
        public decimal Principal { get; set; }
        public System.DateTime DateMade { get; set; }
        public string MadeBy { get; set; }
        public WithdrawStatus WithdrawStatus { get; set; }
    }
    public enum WithdrawStatus
    {
        NotMatched,
        Matched
    }
   public  class MatchViewModel
    {
        public System.Guid Id { get; set; }
        public decimal Amount { get; set; }
        public System.Guid DepositId { get; set; }
        public System.Guid WithdrawalId { get; set; }
        public string PayedBy { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime DueDate { get; set; }
        public bool status { get; set; }
       
     
        public List<DepositModel> DepositModels { get; set; }
    }
    public class DepositModel
    {
        public System.Guid Id { get; set; }
        public decimal Amount { get; set; }
        public string DepositedBy { get; set; }
        public bool Match { get; set; }
        public System.DateTime ExpectedPaymentDate { get; set; }
    }
    public class PayViewModel
    {
        public List<MatchViewModel> MatchViewModels { get; set; }
    }
    public class ContactSupportViewModel
    {
        public System.Guid Id { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public string SentBy { get; set; }
        public string Reply { get; set; }
        public System.DateTime DateSent { get; set; }
        public Nullable<System.DateTime> DateReplied { get; set; }
        public Status Status { get; set; }

        
    }
    public enum Status
    {
        New,
        Pending,
        Replied
    }

}