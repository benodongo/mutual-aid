﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MutualAid.Models.Home
{
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string NewPassword { get; set; }
        public string RepeatPassword { get; set; }
    }
    public class RegisterViewModel
    {
        public string Fullname { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string RepeatPassword { get; set; }
        public string Email { get; set; }
        public Role Role { get; set; }
        public bool Active { get; set; }
    }
    public enum Role
    {
        Admin,
        Member
    }
    public class MailModel
    {
        public string From
        {
            get;
            set;
        }
        public string To
        {
            get;
            set;
        }
        public string Subject
        {
            get;
            set;
        }
        public string Body
        {
            get;
            set;
        }
    }
}