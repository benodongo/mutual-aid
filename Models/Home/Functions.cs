﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MutualAid.Models.Home
{
    public class Functions
    {
        public static string Encrypt(string password)
        {
            var salt = BCrypt.Net.BCrypt.GenerateSalt();
            string passwordHash = BCrypt.Net.BCrypt.HashPassword(password, "$2a$10$IKmTt3y83y5doeC/L6JEgu");
            return passwordHash;
        }
    }
}