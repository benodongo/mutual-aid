//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MutualAid
{
    using System;
    using System.Collections.Generic;
    
    public partial class Withdraw
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Withdraw()
        {
            this.PayReceives = new HashSet<PayReceive>();
        }
    
        public System.Guid Id { get; set; }
        public System.Guid DepositId { get; set; }
        public decimal Amount { get; set; }
        public System.DateTime DateMade { get; set; }
        public string MadeBy { get; set; }
        public int WithdrawStatus { get; set; }
        public bool IsWithdrawn { get; set; }
    
        public virtual Deposit Deposit { get; set; }
        public virtual SystemUser SystemUser { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PayReceive> PayReceives { get; set; }
    }
}
