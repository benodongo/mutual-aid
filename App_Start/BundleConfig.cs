﻿using System.Web;
using System.Web.Optimization;

namespace MutualAid
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                 "~/Content/assets/plugins/jquery/jquery.min.js",
                  "~/Content/assets/plugins/jquery-ui/jquery-ui.js",
                   "~/Content/assets/plugins/popper/popper.js",
                    "~/Content/assets/plugins/bootstrap/js/bootstrap.min.js",
                     "~/Content/assets/plugins/pace/pace.min.js",
                     "~/Content/assets/js/jquery.slimscroll.min.js",
                      "~/Content/assets/plugins/datatables/jquery.dataTables.min.js",
                       "~/Content/assets//plugins/datatables/responsive/dataTables.responsive.js",
                        "~/Content/assets/plugins/datatables/extensions/dataTables.jqueryui.min.js",
                         "~/Content/assets/plugins/simpler-sidebar/jquery.simpler-sidebar.min.js",
                        "~/Content/assets/plugins/modal/ui-modals.js",
                            "~/Content/assets/plugins/sweetalert/bootstrap-sweetalert.js",
                          "~/Content/assets/js/dashboard/analytics-dashboard-init.js",          
                           "~/Content/assets/js/highlight.min.js",
                            "~/Content/assets/js/app.js",
                              "~/Content/assets/js/custom.js"
                      ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/assets/plugins/bootstrap/css/bootstrap.min.css",
                      "~/Content/assets/plugins/font-awesome/css/font-awesome.min.css",
                        "~/Content/assets/plugins/simple-line-icons/css/simple-line-icons.css",
                          "~/Content/assets/plugins/ionicons/css/ionicons.css",
                            "~/Content/assets/plugins/jquery-ui/jquery-ui.css",
                                "~/Content/assets/plugins/datatables/jquery.dataTables.min.css",
                                    "~/Content/assets/plugins/datatables/extensions/dataTables.jqueryui.min.css",
                                    //"~/Content/assets/plugins/datepicker/css/datepicker.min.css",
                                        "~/Content/assets/plugins/sweetalert/bootstrap-sweetalert.css",
                                //            "~/Content/assets/plugins/apex-chart/apexcharts.css",
                                //"~/Content/assets/plugins/themify-icons/themify-icons.css",
                                   "~/Content/assets/css/app.min.css",
                              "~/Content/assets/css/style.min.css",
                               "~/Content/assets/css/style.css"));
        }
    }
}
