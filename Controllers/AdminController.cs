﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MutualAid.Models.Admin;
using MutualAid.Models.Transaction;
using System.Data.Entity;

namespace MutualAid.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var withdrawals = db.Withdraws.Where(p => p.IsWithdrawn == false && p.WithdrawStatus == 0).ToList();
                    if (withdrawals != null)
                    {
                        foreach (var item in withdrawals)
                        {
                            var EachWith = db.Withdraws.FirstOrDefault(p => p.Id == item.Id);
                            var matches = db.PayReceives.Where(p => p.WithdrawalId == item.Id).ToList();
                            decimal total_amount = 0;
                            foreach (var item2 in matches)
                            {
                                total_amount = total_amount + item2.Amount;
                            }
                            if (total_amount == item.Amount)
                            {
                                EachWith.WithdrawStatus = 1;
                                db.Entry(EachWith).State = EntityState.Modified;
                                db.SaveChanges();
                            }
                        }
                    }
                 
                }
                return View();
            }
        
        }
        //rate
        public ActionResult NewRate()
        {
           
            using (var db = new MutualAidEntities())
            {
                var rates = db.Rates.ToList();
                if (rates == null)
                {
                    return View(new RateViewModel());
                }
                else
                {
                    ViewBag.Error = "Rate is already set.Choose edit to change the rate";
                    return RedirectToAction("Rates", "Admin");
                }
            }

        }
        [HttpPost]
        public ActionResult NewRate(RateViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var rate = new Rate
                    {
                        Id = Guid.NewGuid(),
                        BonusRate = model.BonusRate,
                        AddedBy = sessionUser.Email,
                        DateAdded = DateTime.Now
                    };
                    db.Rates.Add(rate);
                    db.SaveChanges();
                    return RedirectToAction("Rates", "Admin");
                }
            }
        }
        public ActionResult Rates()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var rates = db.Rates.ToList();
                    return View(rates);
                }
            }
        }
        public ActionResult EditRate(Guid id)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                RateViewModel model = new RateViewModel();
                using (var db = new MutualAidEntities())
                {
                    var rate = db.Rates.FirstOrDefault(p => p.Id == id);
                    model.BonusRate = rate.BonusRate;
                    return View(model);
                }
            }
        }
        [HttpPost]
        public ActionResult EditRate(RateViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var rate = db.Rates.FirstOrDefault(p => p.Id == model.id);
                    rate.BonusRate = model.BonusRate;
                    rate.DateAdded = DateTime.Now;
                    db.Entry(rate).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Rates", "Admin");
                }
            }
        }
        //set up
        public ActionResult NewSetUp()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
              
                return View(new SetUpViewModel());
            }
        }
            
        [HttpPost]
        public ActionResult NewSetUp(SetUpViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var old_setup = db.Setups.FirstOrDefault();
                    if (old_setup != null)
                    {
                        ViewBag.Error = "Only one set up can be made at a time, edit the existing ones";
                        return View(model);
                    }
                    else
                    {
                        var setup = new Setup
                        {
                            Id = Guid.NewGuid(),
                            DepositMin = model.DepositMin,
                            DepositMax = model.DepositMax,
                            MaturityDuration = model.MaturityDuration,
                            WithrawalConfirmDuration = model.WithrawalConfirmDuration,
                            PayDuration = model.PayDuration,
                            AddedBy = sessionUser.Email,
                            DateAdded = DateTime.Now
                        };
                        db.Setups.Add(setup);
                        db.SaveChanges();
                        return RedirectToAction("SetUps", "Admin");
                    }
                   
                }
            }
        }
        public ActionResult SetUps()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var setups = db.Setups.ToList();
                    return View(setups);
                }
            }
        }
        public ActionResult EditSetUp(Guid id)
        {
            SetUpViewModel model = new SetUpViewModel();
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var setup = db.Setups.FirstOrDefault(p => p.Id == id);
                    model.DepositMin = setup.DepositMin;
                    model.DepositMax = setup.DepositMax;
                    model.MaturityDuration = setup.MaturityDuration;
                    model.PayDuration = setup.PayDuration;
                    model.WithrawalConfirmDuration = setup.WithrawalConfirmDuration;
                    return View(model);
                }
            }
        }
        [HttpPost]
        public ActionResult EditSetUp(SetUpViewModel model)
        {
           
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var setup = db.Setups.FirstOrDefault(p => p.Id == model.Id);
                    setup.DepositMin = model.DepositMin;
                    setup.DepositMax = model.DepositMax;
                    setup.MaturityDuration = model.MaturityDuration;
                    setup.PayDuration = model.PayDuration;
                    setup.WithrawalConfirmDuration = model.WithrawalConfirmDuration;
                    setup.DateAdded = DateTime.Now;
                    db.Entry(setup).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("SetUps", "Admin");
                }
            }
        }
        public ActionResult DelSetUp(Guid id)
        {
            using (var db = new MutualAidEntities())
            {
                var setup = db.Setups.FirstOrDefault(p => p.Id == id);
                db.Setups.Remove(setup);
                db.SaveChanges();
                return RedirectToAction("SetUps", "Admin");
            }
        }
        #region reports
        public ActionResult AllDeposits()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");

            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var deposits = db.Deposits.OrderByDescending(p => p.DateDeposited).ToList();
                    return View(deposits);
                }
            }
        }
        public ActionResult AllWithdrawals()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");

            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var deposits = db.Withdraws.OrderByDescending(p => p.DateMade).ToList();
                    return View(deposits);
                }
            }
        }
        public ActionResult PendingDeposits()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");

            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var deposits = db.Deposits.Where(p=>p.IsPaid == false).OrderByDescending(p => p.DateDeposited).ToList();
                    return View(deposits);
                }
            }
        }
        public ActionResult DisputedDeposits()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");

            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var deposits = db.PayReceives.Where(p=>p.Disputed == true && p.IsPayed == false).OrderByDescending(p => p.ExpectedPaymentDate).ToList();
                    return View(deposits);
                }
            }
        }
        public ActionResult DelayedPayments()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");

            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var today = DateTime.Now;
                    var deposits = db.PayReceives.Where(p =>  p.IsPayed == false && (p.ExpectedPaymentDate < today)).OrderByDescending(p => p.ExpectedPaymentDate).ToList();
                    return View(deposits);
                }
            }
        }
        public ActionResult Suspend(string email)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");

            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var user = db.SystemUsers.FirstOrDefault(p => p.Email == email);
                    user.Active = false;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("DelayedPayments", "Admin");
                }
            }
        }
        public ActionResult ConfirmPay(Guid id)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");

            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var pay = db.PayReceives.FirstOrDefault(p => p.Id == id);
                    pay.IsPayed = true;
                    pay.ActualDateReceived = DateTime.Now;
                    db.Entry(pay).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("DelayedPayments", "Admin");
                }
            }
        }
        public ActionResult Contacts()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var supports = db.ContactSupports.OrderByDescending(p => p.DateSent).ToList();
                    return View(supports);
                }
            }
        }
  
        public ActionResult Reply(Guid id)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var support = db.ContactSupports.FirstOrDefault(p => p.Id == id);
                    ContactSupportViewModel model = new ContactSupportViewModel();
                    model.Id = support.Id;
                    model.Message = support.Message;
                    model.Title = support.Title;
                    return View(model);
                }
            }
        }
        [HttpPost]
        public ActionResult Reply(ContactSupportViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var support = db.ContactSupports.FirstOrDefault(p => p.Id == model.Id);
                    support.Reply = model.Reply;
                    support.Status = 2;
                    db.Entry(support).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Contacts", "Admin");
                
                  
                }
            }
        }
        #endregion
    }


}