﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CaptchaMvc.HtmlHelpers;
using MutualAid.Models.Transaction;
using System.Data.Entity;

namespace MutualAid.Controllers
{
    public class TransactionController : Controller
    {
       public ActionResult NewDeposit()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return View(new DepositViewModel());
            }
        }
        [HttpPost]
        public ActionResult NewDeposit(DepositViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (this.IsCaptchaValid("Validate your captcha"))
                {
                    using (var db = new MutualAidEntities())
                    {
                        var setup = db.Setups.FirstOrDefault();
                        var check = model.Amount % 1000;
                        if (model.Amount < setup.DepositMin || model.Amount > setup.DepositMax)
                        {
                            ViewBag.Message = "The amount entered should be between Ksh" + setup.DepositMin + " and Ksh. " + setup.DepositMax;
                            return View(model);
                        }
                     
                        else  if (check != 0)
                        {
                            ViewBag.Message = "Your Deposit should be in multiple of thousands i.e , 1000, 2000,5000 e.t.c";
                            return View(model);
                        }
                        else
                        {
                            var depo = new Deposit
                            {
                                Id = Guid.NewGuid(),
                                Amount = model.Amount,
                                DepositType = 0,
                                DateDeposited = DateTime.Now,
                                DepositedBy = sessionUser.Email

                            };
                            db.Deposits.Add(depo);
                            db.SaveChanges();
                            return RedirectToAction("Dashboard", "Home");
                        }
                    }
                       
                }
                else
                {
                    ViewBag.ErrMessage = "Enter the right code";
                    return View(model);
                }
                
            }
        }
        #region withdraw
        public ActionResult Withdraw()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                WithdrawViewModel model = new WithdrawViewModel();
                var deposit = new Deposit();
                var rate = new Rate();
                var principal = new decimal();
                var redepo = new decimal();
                if (sessionUser != null)
                {
                    using (var db = new MutualAidEntities())
                    {
                        deposit = db.Deposits.FirstOrDefault(p => p.DepositedBy == sessionUser.Email);
                      
                        rate = db.Rates.FirstOrDefault();
                        if (deposit != null)
                        {
                            var bonus = (rate.BonusRate / 100) * deposit.Amount;
                            principal = deposit.Amount + bonus;
                            redepo = principal / 2;
                            model.DepositId = deposit.Id;
                        }

                    }
                }
                model.Redeposit = redepo;
                model.Principal = principal;
            
                return View(model);
            }
        }
        [HttpPost]
        public ActionResult Withdraw(WithdrawViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var redepo = model.Principal / 2;
                    var setup = db.Setups.FirstOrDefault();
                    var check = model.Redeposit % 1000;
                    if (model.Redeposit < redepo)
                    {
                        ViewBag.Error = "Redeposit cannot be lower than half the amount withdrawable";
                        return View(model);
                    }
                    
                     else if (model.Redeposit < setup.DepositMin || model.Redeposit > setup.DepositMax)
                    {
                        ViewBag.Message = "The amount entered should not exceeed and Ksh. " + setup.DepositMax;
                        return View(model);
                    }
                    
                    else if (check != 0)
                    {
                        ViewBag.Message = "Your Deposit should be in multiple of thousands i.e , 1000, 2000,5000 e.t.c";
                        return View(model);
                    }
                    else
                    {
                        var withdraw = new Withdraw
                        {
                            Id = Guid.NewGuid(),
                            Amount = model.Principal,
                            DepositId = model.DepositId,
                            MadeBy = sessionUser.Email,
                            DateMade = DateTime.Now,
                            WithdrawStatus = 0
                        };
                        db.Withdraws.Add(withdraw);
                        var depo = new Deposit
                        {
                            Id = Guid.NewGuid(),
                            Amount = model.Redeposit,
                            DepositType = 1,
                            DateDeposited = DateTime.Now,
                            DepositedBy = sessionUser.Email
                        };
                        db.Deposits.Add(depo);
                        db.SaveChanges();
                        return RedirectToAction("Dashboard", "Home");
                    }

                    
                }
            }
        }
        public ActionResult GenerateWithdrawal()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else if (sessionUser != null && sessionUser.Role == 0)
            {
                return View(new WithdrawViewModel());
            }
            else
            {
                Session["Access"] = "Access Denied";
                return RedirectToAction("Index", "Admin");
            }
        
        }
        [HttpPost]
        public ActionResult GenerateWithdrawal(WithdrawViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {

                using (var db = new MutualAidEntities())
                {
                    var adminDepo = model.Amount / 2;
                    var depo = new Deposit
                    {
                        Id = Guid.NewGuid(),
                        Amount = adminDepo,
                        DepositType = 0,
                        DateDeposited = DateTime.Now,
                        DepositedBy = sessionUser.Email,
                        IsPaid = true,
                        IsMature = true,
                    };
                    db.Deposits.Add(depo);
                    var withdraw = new Withdraw
                    {
                        Id = Guid.NewGuid(),
                        Amount = model.Amount,
                        DepositId = depo.Id,
                        MadeBy = sessionUser.Email,
                        DateMade = DateTime.Now,
                        WithdrawStatus = 0
                    };
                    db.Withdraws.Add(withdraw);
                    db.SaveChanges();
                    return RedirectToAction("Index", "Admin");
                }
            }
        }
        #endregion
        #region match pay and receive
        public ActionResult Withdrawals()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var withdrawals = db.Withdraws.Where(p => p.WithdrawStatus == 0).OrderBy(p=>p.DateMade).ToList();
                    return View(withdrawals);
                }
            }
        }
        public ActionResult MatchRequest(Guid id)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                MatchViewModel model = new MatchViewModel();
                model.DepositModels = new List<DepositModel>();
                using (var db = new MutualAidEntities())
                {
                    var withdrawal = db.Withdraws.FirstOrDefault(p => p.Id == id);
                    model.WithdrawalId = id;
                    model.ReceivedBy = withdrawal.MadeBy;
                    var admin = db.SystemUsers.FirstOrDefault(p => p.Role == 0);
                    var depositors = db.Deposits.Where(p => p.IsPaid == false && p.Id != id && (p.DepositedBy != withdrawal.MadeBy && p.DepositedBy != admin.Email)).Take(5).ToList();
                    foreach (var item in depositors)
                    {
                        var depo = new DepositModel
                        {
                            Id = item.Id,
                            Amount = item.Amount,
                            DepositedBy = item.DepositedBy,
                            Match = false,
                            ExpectedPaymentDate = new DateTime(),
                        };
                        model.DepositModels.Add(depo);
                    }
                    return View(model);
                }
            }
        }
        [HttpPost]
        public ActionResult MatchRequest (MatchViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var payduration = db.Setups.FirstOrDefault().PayDuration;
                    DateTime today = DateTime.Now;
                    DateTime payBy = today.AddHours(payduration);
                    for (int i = 0; i < model.DepositModels.Count; i++)
                    {
                        if (model.DepositModels[i].Match == true)
                        {
                            var match = new PayReceive
                            {
                                Id = Guid.NewGuid(),
                                Amount = model.DepositModels[i].Amount,
                                DepositId = model.DepositModels[i].Id,
                                WithdrawalId = model.WithdrawalId,
                                PayedBy = model.DepositModels[i].DepositedBy,
                                ReceivedBy = model.ReceivedBy,
                                ExpectedPaymentDate = payBy
                                //ExpectedPaymentDate = model.DepositModels[i].ExpectedPaymentDate
                            };
                            db.PayReceives.Add(match);

                        }

                    }
                    db.SaveChanges();
                    return RedirectToAction("Withdrawals", "Transaction");
                }
            }
        }
        public ActionResult PayTo()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                PayViewModel model = new PayViewModel();
                model.MatchViewModels = new List<MatchViewModel>();
                using (var db = new MutualAidEntities())
                {
                    var pendings = db.PayReceives.Where(p => p.IsPayed == false && p.PayedBy == sessionUser.Email).ToList();
                    foreach (var item in pendings)
                    {
                        var due = new MatchViewModel
                        {
                            Id = item.Id,
                            Amount = item.Amount,
                            DepositId = item.DepositId,
                            WithdrawalId = item.WithdrawalId,
                            ReceivedBy = item.ReceivedBy,
                            DueDate = item.ExpectedPaymentDate

                        };
                        model.MatchViewModels.Add(due);
                    }
                    return View(model);
                }
            }
        }
        [HttpPost]
        public ActionResult PayTo(PayViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    foreach (var item in model.MatchViewModels.Where(p => p.status == true))
                    {
                        var payrec = db.PayReceives.FirstOrDefault(p => p.Id == item.Id);
                        payrec.ActualDatePayed = DateTime.Now;
                        db.Entry(payrec).State = EntityState.Modified; 
                    }
                    db.SaveChanges();
                    return View(model);
                }
            }
        }

        public ActionResult ReceiveFrom()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                PayViewModel model = new PayViewModel();
                model.MatchViewModels = new List<MatchViewModel>();
                using (var db = new MutualAidEntities())
                {
                    var pendings = db.PayReceives.Where(p => p.IsPayed == false && p.ReceivedBy == sessionUser.Email).ToList();
                    foreach (var item in pendings)
                    {
                        var due = new MatchViewModel
                        {
                            Id = item.Id,
                            Amount = item.Amount,
                            DepositId = item.DepositId,
                            WithdrawalId = item.WithdrawalId,
                            PayedBy = item.PayedBy,
                            DueDate = item.ExpectedPaymentDate,
                            

                        };
                        model.MatchViewModels.Add(due);
                    }
                    return View(model);
                }
            }
        }
        [HttpPost]
        public ActionResult ReceiveFrom(PayViewModel model, string submitButton)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    switch (submitButton)
                    {
                        case "Confirm":
                            foreach (var item in model.MatchViewModels.Where(p => p.status == true))
                            {
                                var payrec = db.PayReceives.FirstOrDefault(p => p.Id == item.Id);
                                payrec.ActualDateReceived = DateTime.Now;
                                payrec.IsPayed = true;
                                db.Entry(payrec).State = EntityState.Modified;
                            }
                            break;
                        case "Fake Teller":
                            foreach (var item in model.MatchViewModels.Where(p => p.status == true))
                            {
                                var payrec = db.PayReceives.FirstOrDefault(p => p.Id == item.Id);
                                payrec.Disputed = true;
                                var payee = db.SystemUsers.FirstOrDefault(p => p.Email == item.ReceivedBy);
                                payee.Active = false;
                                var payer = db.SystemUsers.FirstOrDefault(p => p.Email == item.PayedBy);
                                payer.Active = false;
                                db.Entry(payee).State = EntityState.Modified;
                                db.Entry(payer).State = EntityState.Modified;
                                db.Entry(payrec).State = EntityState.Modified;
                            }
                            break;
                    }
                   
                    db.SaveChanges();
                    return View(model);
                }
            }
        }
        #endregion


    }
}