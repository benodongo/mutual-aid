﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MutualAid.Models.Home;
using MutualAid.Models.Transaction;
using System.Web.Hosting;
using System.Threading;
using System.Threading.Tasks;
using System.Net.Mail;

namespace MutualAid.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Dashboard()
        {
            //TODO
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var currentDeposit = db.Deposits.FirstOrDefault(p => p.DepositedBy == sessionUser.Email && p.IsWithdrawn == false && p.IsMature == false && p.IsPaid == false);
                    if (currentDeposit != null)
                    {
                        var Payments = db.PayReceives.Where(p => p.PayedBy == sessionUser.Email && p.DepositId == currentDeposit.Id).ToList();
                        var PayTrue = 0;
                        var PayFalse = 0;
                        foreach (var item in Payments)
                        {
                            if (item.IsPayed == true)
                            {
                                PayTrue++;
                            }
                            else
                            {
                                PayFalse++;
                            }
                        }
                        if (PayTrue >0 && PayFalse <= 0)
                        {
                            var matureduration = db.Setups.FirstOrDefault().MaturityDuration;
                            DateTime today = DateTime.Now;
                            DateTime matureBy = today.AddHours(matureduration);
                            currentDeposit.IsPaid = true;
                            currentDeposit.MatureBy = matureBy;
                            db.Entry(currentDeposit).State = EntityState.Modified;
                            db.SaveChanges();
                        }
                      
                    }
                    var withdrawal = db.Withdraws.FirstOrDefault(p => p.MadeBy == sessionUser.Email && p.WithdrawStatus == 0);
                    if (withdrawal != null)
                    {
                        var withdrawals = db.PayReceives.Where(p => p.ReceivedBy == sessionUser.Email && p.WithdrawalId == withdrawal.Id).ToList();
                        var withTrue = 0;
                        var withFalse = 0;
                        foreach (var item in withdrawals)
                        {
                            if (item.IsPayed == true)
                            {
                                withTrue++;
                            }
                            else
                            {
                                withFalse++;
                            }
                        }
                        if (withTrue >0 && withFalse <= 0)
                        {
                            withdrawal.IsWithdrawn = true;
                            var originalDepo = db.Deposits.FirstOrDefault(p => p.Id == withdrawal.DepositId);
                            originalDepo.IsWithdrawn = true;
                            db.Entry(withdrawal).State = EntityState.Modified;
                            db.Entry(originalDepo).State = EntityState.Modified;
                            db.SaveChanges();

                        }
                    }
                    DateTime now = DateTime.Now;
                    var pays = db.PayReceives.Where(p => p.PayedBy == sessionUser.Email && p.IsPayed == false && p.ExpectedPaymentDate < now).Count();
                    if (pays > 0)
                    {
                        var myuser = db.SystemUsers.FirstOrDefault(p => p.Email == sessionUser.Email);
                        myuser.Active = false;
                        db.Entry(myuser).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                  
                }
                return View();
            }
            
        }

    

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        #region Authentication
        public ActionResult Signin()
        {
            return View(new LoginViewModel());
        }
        [HttpPost]
        public ActionResult Signin(LoginViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    using (var db = new MutualAidEntities())
                    {
                        var user = db.SystemUsers.Find(model.Email);
                        if (user.Active == false && user.FirstLogin == true)
                        {
                            ViewBag.Error = "The Account is suspended . Contact Admininstrator for help";
                            return View(model);
                        }
                        if (user.Active == false && user.FirstLogin == false)
                        {
                            ViewBag.Error = "Your account has not been activated, please wait or contact administrator for help";
                            return View(model);
                        }
                        if (user != null && user.Active == true )
                        {
                            var pass = Functions.Encrypt(model.Password);
                            if (pass != user.Password)
                            {
                                ViewBag.Error = "The password entered is not correct";
                            }
                            else
                            {
                                user.LastLogin = DateTime.Now;
                                db.Entry(user).State = EntityState.Modified;
                                db.SaveChanges();
                                Session["User"] = user;
                                if (user.Role == 0)
                                {
                                    return RedirectToAction("Index", "Admin");
                                }
                                else
                                {
                                    return RedirectToAction("Dashboard", "Home");
                                }
                               
                            }
                        }
                        else
                        {
                            ViewBag.Error = "The Email or Password is incorrect";
                            return View(model);
                        }

                    }

                }
                else
                {
                    ViewBag.Error = "Invalid Login Data check if you provided a valid email address or password";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                ViewBag.Error = "Invalid Login Data check if you provided a valid email address or password";
            }
            return View(model);
        }
        public ActionResult ChangePassword()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var user = db.SystemUsers.FirstOrDefault(p => p.Email == sessionUser.Email);
                    LoginViewModel model = new LoginViewModel();
                    model.Email = user.Email;
                    return View(model);
                }
            }
        }
        [HttpPost]
        public ActionResult ChangePassword(LoginViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var user = db.SystemUsers.FirstOrDefault(p => p.Email == sessionUser.Email);
                    var old_password = Functions.Encrypt(model.Password);
                    var new_password = Functions.Encrypt(model.NewPassword);
                    if (old_password != user.Password)
                    {
                        ViewBag.Error = "The initial Password entered is wrong";
                        return View(model);
                    }
                    else
                    if (model.NewPassword != model.RepeatPassword)
                    {
                        ViewBag.Error = "Passwords does not match";
                        return View(model);
                    }
                    else
                    if ((old_password == user.Password) && (model.NewPassword == model.RepeatPassword))
                    {
                        user.Password = new_password;
                        db.Entry(user).State = EntityState.Modified;
                        db.SaveChanges();
                        ViewBag.Success = "Accout details Changed Successfully";
                   
                       
                    }
                    return View(model);
                
                }
               
            }
        }
        public ActionResult Logout()
        {
            Session["User"] = null;
            return RedirectToAction("Index", "Home");
        }
        public ActionResult Signup()
        {
            return View(new RegisterViewModel());
        }
        [HttpPost]
        public ActionResult Signup(RegisterViewModel model)
        {
            var hashedPassword = Functions.Encrypt(model.Password);
            var mobile_phone = $"+254{model.Phone}";
            using (var db = new MutualAidEntities())
            {
                var checkuser = db.SystemUsers.Find(model.Email);
                if (checkuser != null)
                {
                    ViewBag.Message = "The Email already exists in the system. Use a different email";
                    return View(model);
                }
                if (model.Password != model.RepeatPassword)
                {
                    ViewBag.Message = "The Passwords not match";
                    return View(model);
                }
                else
                {
                    var user = new SystemUser
                    {
                        FullName = model.Fullname,
                        Email = model.Email,
                        Phone = mobile_phone,
                        Password = hashedPassword,
                        Role = 1
                    };
                    db.SystemUsers.Add(user);
                    db.SaveChanges();
                    ViewBag.Message = "Your registration is successful, now login";
                    return RedirectToAction("Signin", "Home");
                }
                
            }
        }
        public ActionResult Users()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var users = db.SystemUsers.Where(p=>p.Role != 0).ToList();
                    return View(users);
                }
            }
        }
        public ActionResult EditUser(string Email)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                RegisterViewModel model = new RegisterViewModel();
                using (var db = new MutualAidEntities())
                {
                    var user = db.SystemUsers.FirstOrDefault(p => p.Email == Email);
                    model.Fullname = user.FullName;
                    model.Email = user.Email;
                    model.Phone = user.Phone;
                    model.Role = (Role)user.Role;
                    model.Active = user.Active;
                    
                    return View(model);
                }
            }
        }
        [HttpPost]
        public ActionResult EditUser(RegisterViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                 
                    var user = db.SystemUsers.FirstOrDefault(p => p.Email == model.Email);
                    user.FullName = model.Fullname;
                    user.Email = model.Email;
                    user.Phone = model.Phone;
                    if (model.Password != null &&  model.Password != model.RepeatPassword)
                    {
                        ViewBag.Message = "The passwords don't match";
                        return View(model);
                    }
                    if (model.Password != null && model.Password == model.RepeatPassword)
                    {
                       var mypass = Functions.Encrypt(model.Password);
                        user.Password = mypass;
                    }
                    user.Role = (int)model.Role;
                    user.Active = model.Active;
                    user.FirstLogin = true;
                    db.Entry(user).State = EntityState.Modified;
                    db.SaveChanges();
                    return RedirectToAction("Users", "Home");
                }
            }
        }
        public ActionResult RemoveUser(string Email)
        {
            using (var db = new MutualAidEntities())
            {
                var user = db.SystemUsers.FirstOrDefault(p => p.Email == Email);
                user.Active = false;
                db.Entry(user).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Users", "Home");
            }
        }
        #endregion
        #region background processes

        #endregion
        #region terms and conditions
        public ActionResult Terms()
        {
            return View();
        }
        public ActionResult Privacy()
        {
            return View();
        }
        #endregion
        #region transaction
        public ActionResult PayToDashboard()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                PayViewModel model = new PayViewModel();
                model.MatchViewModels = new List<MatchViewModel>();
                using (var db = new MutualAidEntities())
                {
                    var pendings = db.PayReceives.Where(p => p.IsPayed == false && p.PayedBy == sessionUser.Email).ToList();
                    foreach (var item in pendings)
                    {
                        var due = new MatchViewModel
                        {
                            Id = item.Id,
                            Amount = item.Amount,
                            DepositId = item.DepositId,
                            WithdrawalId = item.WithdrawalId,
                            ReceivedBy = item.ReceivedBy,
                            DueDate = item.ExpectedPaymentDate,
                             status = item.IsPayed

                        };
                        model.MatchViewModels.Add(due);
                    }
                    return PartialView(model);
                }
            }
        }
        [HttpPost]
        public ActionResult PayToDashboard(PayViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    foreach (var item in model.MatchViewModels.Where(p=>p.status == true))
                    {
                        var payrec = db.PayReceives.FirstOrDefault(p => p.Id == item.Id);
                        payrec.ActualDatePayed = DateTime.Now;
                        db.Entry(payrec).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    return RedirectToAction("Dashboard", "Home");
                }
            }
        }
        public ActionResult ReceiveFromDash()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                PayViewModel model = new PayViewModel();
                model.MatchViewModels = new List<MatchViewModel>();
                using (var db = new MutualAidEntities())
                {
                    var pendings = db.PayReceives.Where(p => p.IsPayed == false && p.ReceivedBy == sessionUser.Email).ToList();
                    foreach (var item in pendings)
                    {
                        var due = new MatchViewModel
                        {
                            Id = item.Id,
                            Amount = item.Amount,
                            DepositId = item.DepositId,
                            WithdrawalId = item.WithdrawalId,
                            PayedBy = item.PayedBy,
                            DueDate = item.ExpectedPaymentDate,
                            status = item.IsPayed

                        };
                        model.MatchViewModels.Add(due);
                    }
                    return PartialView(model);
                }
            }
        }
        [HttpPost]
        public ActionResult ReceiveFromDash(PayViewModel model, string submitButton)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    switch (submitButton)
                    {
                        case "Confirm":
                            foreach (var item in model.MatchViewModels.Where(p => p.status == true))
                            {
                                var payrec = db.PayReceives.FirstOrDefault(p => p.Id == item.Id);
                                payrec.ActualDateReceived = DateTime.Now;
                                payrec.IsPayed = true;
                                db.Entry(payrec).State = EntityState.Modified;
                            }
                            break;
                        case "Fake Teller":
                            foreach (var item in model.MatchViewModels.Where(p => p.status == true))
                            {
                                var payrec = db.PayReceives.FirstOrDefault(p => p.Id == item.Id);
                                payrec.Disputed = true;
                                db.Entry(payrec).State = EntityState.Modified;
                            }
                            break;
                    }
                    db.SaveChanges();
                    return RedirectToAction("Dashboard", "Home");
                }
            }
        }
        #endregion
        #region support
        public ActionResult Supports()
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var supports = db.ContactSupports.Where(p => p.SentBy == sessionUser.Email).OrderByDescending(p => p.DateSent).ToList();
                    return View(supports);
                }
            }
        }
        public ActionResult NewSupport()
        {
            return View(new ContactSupportViewModel());
        }
        [HttpPost]
        public ActionResult NewSupport(ContactSupportViewModel model)
        {
            var sessionUser = Session["User"] as SystemUser;
            if (sessionUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                using (var db = new MutualAidEntities())
                {
                    var sup = new ContactSupport
                    {
                        Id = Guid.NewGuid(),
                        Title = model.Title,
                        Message = model.Message,
                        DateSent = DateTime.Now,
                        SentBy = sessionUser.Email
                    };
                    db.ContactSupports.Add(sup);
                    db.SaveChanges();
                    return RedirectToAction("Supports", "Home");
                }
            }
        }
        #endregion
        #region forgot password
        public ActionResult ForgotPassword()
        {
            MailModel model = new MailModel();
            return View();
        }
        [HttpPost]
        public ActionResult ForgetPassword(MailModel model)
        {
            using (var db = new MutualAidEntities())
            {
                var user = db.SystemUsers.Find(model.To);
                if (user == null)
                {
                    ViewBag.Error = "The email Entered does not exist in the system.Please Enter the right email";
                    return View(model);
                }


                else
                {
                    string password = System.Web.Security.Membership.GeneratePassword(12, 1);
                    MailMessage mail = new MailMessage();
                    mail.To.Add(model.To);
                    mail.From = new MailAddress(model.From);
                    mail.Subject = "PASSWORD RESET";
                    string Body = $"Your new generated password is:{password}, You can change this after login";
                    mail.Body = Body;
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential("info@mutualaid.co.ke", "Mutualaid.@123"); // Enter seders User name and password  
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                    return View(model);
                }
            }
        
        }
        #endregion
    }

}